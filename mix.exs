defmodule MTG.Mixfile do
  use Mix.Project

  def project do
    [app: :mtg,
     version: "0.2.0",
     elixir: "~> 1.3",
     description: description,
     package: package,
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [
      applications: [
        :logger,
        :httpoison,
        :poison,
      ]
    ]
  end

  defp description do
    """
    Client for magicthegathering.io
    """
  end

  defp package do
    [
      name: :mtg,
      links: %{
        "GitLab" => "https://gitlab.com/thijs-wouters/mtg",
      },
      maintainers: [
        "Thijs Wouters",
      ],
      licenses: [
        "MIT"
      ],
    ]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:httpoison, "~> 0.10.0"},
      {:poison, "~> 3.0"},
      {:ex_doc, ">= 0.0.0", only: :dev},
    ]
  end
end
