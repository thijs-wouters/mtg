defmodule MTG.UtilTest do
  use ExUnit.Case
  alias MTG.Util

  describe "MTG.Util.dromedize/1" do
    test "does not capitalize first character" do
      assert Util.dromedize(:test) == "test"
    end

    test "removes all underscores and capitalizes next charachter" do
      assert Util.dromedize(:t_est_ing) == "tEstIng"
    end
  end

  test "underscores the keys" do
    assert Util.underscore_keys(%{
      "foo" => "bar",
      "fooFoo" => "bar",
      "bar" => %{
        "fooFoo" => "bar",
      },
      "barBar" => [
        "bar",
        %{
          "fooFoo" => "bar",
        },
      ],
    }) == %{
      "foo" => "bar",
      "foo_foo" => "bar",
      "bar" => %{
        "foo_foo" => "bar",
      },
      "bar_bar" => [
        "bar",
        %{
          "foo_foo" => "bar",
        },
      ],
    }
  end
end
