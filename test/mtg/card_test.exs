defmodule MTG.CardTest do
  use ExUnit.Case
  alias MTG.Card

  test "get all cards" do
    assert {:ok, cards} = Card.all
    assert Enum.count(cards) > 1
  end

  test "page cards" do
    assert {:ok, cards} = Card.all(page: 5)
    assert Enum.count(cards) == 100
  end

  test "return specific number of cards" do
    assert {:ok, cards} = Card.all(page_size: 5)
    assert Enum.count(cards) == 5
  end

  test "filter all" do
    assert {:ok, cards} = Card.all(set: "aer", subtypes: "warrior,human")
    assert Enum.count(cards) == 5
  end

  test "get spectific card" do
    assert {:ok, card} = Card.get(386616)
    assert card == %Card{
      name: "Narset, Enlightened Master",
      layout: "normal",
      cmc: 6,
      colors: ["White", "Blue", "Red"],
      color_identity: ["W", "U", "R"],
      type: "Legendary Creature — Human Monk",
      supertypes: ["Legendary"],
      types: ["Creature"],
      subtypes: ["Human", "Monk"],
      rarity: "Mythic Rare",
      set: "KTK",
      set_name: "Khans of Tarkir",
      text: "First strike, hexproof\nWhenever Narset, Enlightened Master attacks, exile the top four cards of your library. Until end of turn, you may cast noncreature cards exiled with Narset this turn without paying their mana costs.",
      flavor: nil,
      artist: "Magali Villeneuve",
      number: "190",
      power: "3",
      toughness: "2",
      loyalty: nil,

      id: "2a9bdc9658a08072fa90c602c045eb6a0d94e083",
      names: nil,
      mana_cost: "{3}{U}{R}{W}",
      multiverseid: 386616,
      variations: nil,
      image_url: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=386616&type=card",
      watermark: "Jeskai",
      border: nil,
      timeshifted: nil,
      hand: nil,
      life: nil,
      reserved: nil,
      release_date: nil,
      starter: nil,
      rulings: [
        %Card.Ruling{
          date: "2014-09-20",
          text: "The cards are exiled face up. Casting the noncreature cards exiled this way follows the normal rules for casting those cards. You must follow all applicable timing rules. For example, if one of the exiled cards is a sorcery card, you can cast it only during your main phase while the stack is empty.",
        },
        %Card.Ruling{
          date: "2014-09-20",
          text: "You can't play any land cards exiled with Narset.",
        },
        %Card.Ruling{
          date: "2014-09-20",
          text: "Because you're already casting the card using an alternative cost (by casting it without paying its mana cost), you can't pay any other alternative costs for the card, including casting it face down using the morph ability. You can pay additional costs, such as kicker costs. If the card has any mandatory additional costs, you must pay those.",
        },
        %Card.Ruling{
          date: "2014-09-20",
          text: "If the card has {X} in its mana cost, you must choose 0 as the value for X when casting it.",
        },
        %Card.Ruling{
          date: "2014-09-20",
          text: "Any exiled cards you don't cast remain in exile.",
        },
      ],
      foreign_names: [
        %Card.Name{
          image_url: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=386885&type=card",
          language: "Chinese Simplified",
          multiverseid: 386885,
          name: "悟道大师娜尔施",
        },
        %Card.Name{
          image_url: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=387154&type=card",
          language: "Chinese Traditional",
          multiverseid: 387154,
          name: "悟道大師娜爾施",
        },
        %Card.Name{
          image_url: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=387692&type=card",
          language: "French",
          multiverseid: 387692,
          name: "Narset, maîtresse éclairée",
        },
        %Card.Name{
          image_url: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=387423&type=card",
          language: "German",
          multiverseid: 387423,
          name: "Narset, Erleuchtete Meisterin",
        },
        %Card.Name{
          image_url: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=387961&type=card",
          language: "Italian",
          multiverseid: 387961,
          name: "Narset, Maestra Illuminata",
        },
        %Card.Name{
          image_url: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=388230&type=card",
          language: "Japanese",
          multiverseid: 388230,
          name: "悟った達人、ナーセット",
        },
        %Card.Name{
          image_url: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=388499&type=card",
          language: "Korean",
          multiverseid: 388499,
          name: "깨달음을 얻은 대가 나르셋",
        },
        %Card.Name{
          image_url: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=388768&type=card",
          language: "Portuguese (Brazil)",
          multiverseid: 388768,
          name: "Narset, Mestra Iluminada",
        },
        %Card.Name{
          image_url: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=389037&type=card",
          language: "Russian",
          multiverseid: 389037,
          name: "Нарсет, Просветленная Наставница",
        },
        %Card.Name{
          image_url: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=389306&type=card",
          language: "Spanish",
          multiverseid: 389306,
          name: "Narset, maestra iluminada",
        },
      ],
      printings: ["pPRE", "KTK"],
      original_text: "First strike, hexproof\nWhenever Narset, Enlightened Master attacks, exile the top four cards of your library. Until end of turn, you may cast noncreature cards exiled with Narset this turn without paying their mana costs.",
      original_type: "Legendary Creature — Human Monk",
      legalities: [
        %Card.Legality{
          format: "Commander",
          legality: "Legal",
        },
        %Card.Legality{
          format: "Khans of Tarkir Block",
          legality: "Legal",
        },
        %Card.Legality{
          format: "Legacy",
          legality: "Legal",
        },
        %Card.Legality{
          format: "Modern",
          legality: "Legal",
        },
        %Card.Legality{
          format: "Vintage",
          legality: "Legal",
        },
      ],
      source: nil,
    }
  end
end
