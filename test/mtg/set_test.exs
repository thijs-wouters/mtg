defmodule MTG.SetTest do
  use ExUnit.Case
  alias MTG.Set

  test "get specific set" do
    assert {:ok, aether_revolt} = Set.get("aer")
    assert aether_revolt == %Set{
      name: "Aether Revolt",
      block: "Kaladesh",
      code: "AER",
      gatherer_code: nil,
      old_code: nil,
      magic_cards_info_code: nil,
      release_date: "2017-01-20",
      border: "black",
      expansion: nil,
      online_only: nil,
      booster: [
        [
          "rare",
          "mythic rare"
        ],
        "uncommon",
        "uncommon",
        "uncommon",
        "common",
        "common",
        "common",
        "common",
        "common",
        "common",
        "common",
        "common",
        "common",
        "common",
        "land",
        "marketing"
      ],
    }
  end
end
