# MTG SDK

Client application for magicthegathering.io

## Installation

1. Add `mtg` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:mtg, "~> 0.1.0"}]
    end
    ```

2. Ensure `mtg` is started before your application:

    ```elixir
    def application do
      [applications: [:mtg]]
    end
    ```

## Usage

### Card lookup

#### All cards
This will return up to 100 cards at a time.

```elixir
MTG.Card.all
```

#### Paging

```elixir
MTG.Card.all(page: 5, page_size: 50)
```

#### Filter

You can filter on certain parameters.

```elixir
MTG.Card.all(set: "aer", subtypes: "warrior,human")
```

#### Get specific card

You can retrieve one card based on multiverse id.

```elixir
MTG.Card.get(386616)
```

### Set lookup

#### Get set by code

You can retrieve the set by code.

```elixir
MTG.Set.get("aer")
```
