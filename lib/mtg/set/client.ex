defmodule MTG.Set.Client do
  use HTTPoison.Base

  defp process_url(url) do
    "https://api.magicthegathering.io/v1/sets" <> url
  end

  defp process_response_body(body) do
    case Poison.Parser.parse(body) do
      {:ok, body} -> MTG.Util.underscore_keys(body)
    end
  end
end
