defmodule MTG.Set do
  defstruct [
    :name,
    :block,
    :code,
    :gatherer_code,
    :old_code,
    :magic_cards_info_code,
    :release_date,
    :border,
    :expansion,
    :online_only,
    :booster,
  ]

  alias __MODULE__
  alias MTG.Set.Client

  def get(code) do
    {:ok, %{body: %{"set" => set}}} = Client.get("/#{code}")
    {:ok, Poison.Decode.decode(set, as: %Set{})}
  end
end
