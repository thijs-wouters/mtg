defmodule MTG.Card.Ruling do
  defstruct [:date, :text]
end
