defmodule MTG.Card.Legality do
  defstruct [:format, :legality]
end
