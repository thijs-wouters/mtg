defmodule MTG.Card.Name do
  defstruct [:image_url, :language, :multiverseid, :name]
end
