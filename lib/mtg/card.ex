defmodule MTG.Card do
  defstruct [
    :name,
    :layout,
    :cmc,
    :colors,
    :color_identity,
    :type,
    :supertypes,
    :types,
    :subtypes,
    :rarity,
    :set,
    :set_name,
    :text,
    :flavor,
    :artist,
    :number,
    :power,
    :toughness,
    :loyalty,

    :id,
    :names,
    :mana_cost,
    :multiverseid,
    :variations,
    :image_url,
    :watermark,
    :border,
    :timeshifted,
    :hand,
    :life,
    :reserved,
    :release_date,
    :starter,
    :rulings,
    :foreign_names,
    :printings,
    :original_text,
    :original_type,
    :legalities,
    :source,
  ]
  alias MTG.Card.Client
  alias __MODULE__

  import MTG.Util

  def all(filters \\ []) do
    query_string = filters
    |> Enum.map(fn({field, value}) -> "#{dromedize(field)}=#{value}" end)
    |> Enum.join("&")

    {:ok, %{body: %{"cards" => cards}}} = Client.get("?#{query_string}")
    {:ok, cards |> Poison.Decode.decode(as: [decode_as])}
  end

  def get(multiverse_id) do
    {:ok, %{body: %{"card" => card}}} = Client.get("/#{multiverse_id}")
    {
      :ok,
      card
      |> Poison.Decode.decode(as: decode_as)
    }
  end

  defp decode_as, do: %Card{
    rulings: [%Card.Ruling{}],
    foreign_names: [%Card.Name{}],
    legalities: [%Card.Legality{}],
  }
end
