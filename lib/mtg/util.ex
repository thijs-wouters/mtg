defmodule MTG.Util do
  def underscore_keys(map) when is_map(map) do
    map
    |> Enum.map(fn({key, value}) -> {Macro.underscore(key), underscore_keys(value)} end)
    |> Enum.into(%{})
  end
  def underscore_keys(list) when is_list(list) do
    list
    |> Enum.map(&underscore_keys/1)
  end
  def underscore_keys(other) do
    other
  end

  def dromedize(atom) do
    to_string(atom)
    |> String.split("_")
    |> Enum.with_index
    |> Enum.map(fn
      {part, 0} -> part
      {part, _} -> String.capitalize(part)
    end)
    |> Enum.join
  end
end
